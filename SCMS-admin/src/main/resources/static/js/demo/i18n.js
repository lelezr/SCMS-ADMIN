
/**
 * 设置语言类型： 默认为中文
 */
var i18nLanguage = "zh-CN";

/*
设置一下网站支持的语言种类
 */
var webLanguage = ['zh-CN', 'zh-TW', 'en'];

$(document).ready(function() {

    // execI18n();
    var navLanguage = getNavLanguage();
    if (navLanguage) {
        // 判断是否在网站支持语言数组里
        var charSize = $.inArray(navLanguage, webLanguage);
        if (charSize > -1) {
            i18nLanguage = 'en';//navLanguage;
        };
    }
    //保存到cookie  _locale key 跟jfinal 默认一致
    setCookie("_locale",i18nLanguage,1000*60*60*24*30);
});

/**
 * 获取浏览器语言类型
 * @return {string} 浏览器国家语言
 */
var getNavLanguage = function(){
    if(navigator.appName == "Netscape"){
        return navigator.language.trim();
    }
    return false;
}

/**
 * 执行页面i18n方法
 * @return
 */
var execI18n = function(){
    /*
    获取一下资源文件名
     */
    var optionEle = $("[data-locale]");
    if (optionEle.length < 1) {
        console.log("未找到页面名称元素，请在页面写入\n <meta id=\"i18n_pagename\" content=\"页面名(对应语言包的语言文件名)\">");
        return false;
    };
    // var sourceName = optionEle.attr('content');
    // sourceName = sourceName.split('-');
    /*
    首先获取用户浏览器设备之前选择过的语言类型
     */
    var language = getCookie("language");
    if (language) {
        i18nLanguage = language;
    } else {
        // 获取浏览器语言
        var navLanguage = getNavLanguage();
        if (navLanguage) {
            // 判断是否在网站支持语言数组里
            var charSize = $.inArray(navLanguage, webLanguage);
            if (charSize > -1) {
                i18nLanguage = navLanguage;
                // 存到缓存中
                setCookie("language",navLanguage);
            };
        } else{
            console.log("not navigator");
            return false;
        }
    }
    /* 需要引入 i18n 文件*/
    if ($.i18n == undefined) {
        console.log("请引入i18n js 文件")
        return false;
    };

    /*
    这里需要进行i18n的翻译
     */
    jQuery.i18n.properties({
        name : 'string', //资源文件名称
        path : 'static/i18n/', //资源文件路径
        mode : 'map', //用Map的方式使用资源文件中的值
        language : "en",
        callback : function() {//加载成功后设置显示内容
            var insertEle =  $("[data-locale]");
            console.log(".i18n 写入中...");
           /* insertEle.each(function() {
                // 根据i18n元素的 name 获取内容写入
                // $(this).html($.i18n.prop($(this).attr('name')));
                $(this).html($.i18n.prop($(this).data("locale")));
            });*/
            alert($.i18n.prop("i18n_login"));
            console.log("写入完毕");

           /* console.log(".i18n-input 写入中...");
            var insertInputEle = $(".i18n-input");
            insertInputEle.each(function() {
                var selectAttr = $(this).attr('selectattr');
                if (!selectAttr) {
                    selectAttr = "value";
                };
                $(this).attr(selectAttr, $.i18n.prop($(this).attr('selectname')));
            });
            console.log("写入完毕");*/
        }
    });
}
